/*=====================================================
 S m o o t h   S c r o l l
=====================================================*/
$(function(){
	// Window object
	var $window = $(window);
	// Scroll time
	var scrollTime = 0.5;
	// Distance. Use smaller value for shorter scroll and greater value for longer scroll
	var scrollDistance = 250;
		
	$window.on("mousewheel DOMMouseScroll", function(event){
		
		event.preventDefault();	
										
		var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
		var scrollTop = $window.scrollTop();
		var finalScroll = scrollTop - parseInt(delta*scrollDistance);
			
		TweenMax.to($window, scrollTime, {
			scrollTo : { 
				y:finalScroll, autoKill:true 
			},
			//For more easing functions see http://api.greensock.com/js/com/greensock/easing/package-detail.html
			ease: Power1.easeOut,
			autoKill: false,
			overwrite: 5
		});
	});
});

/*=====================================================
 J C a r o u s e l
 a b c is number of collum set to width >768, >480, <480
===================================================== */

function callJcarousel(object,a,b,c){
	object.on('jcarousel:create jcarousel:reload', function() {
		var element = $(this),
		width = element.innerWidth();
		if (width >= 768) {
			width = width / a;
		} else if (width >= 480) {
			width = width / b;
		} else {
			width = width / c;
		}
		element.jcarousel('items').css('width', width + 'px');
	})
	.jcarousel({
		wrap: 'circular'
	});
	object.parent().find('.jcarousel-control-prev').jcarouselControl({
		target: '-=1'
	});
	object.parent().find('.jcarousel-control-next').jcarouselControl({
		target: '+=1'
	});
}

/*=====================================================
 Animation cart
=====================================================*/
$('.add_to_cart').on('click', function(){
	var cart = $('.fa-shopping-cart');
	// var imgtodrag = $(this).parent().parent().parent().parent().children('.media-container').children('.inner').find('img').eq(0);
	var imgtodrag = $('.main-img img').eq(0);
	console.log(cart);
	if (imgtodrag) {
	var imgclone = imgtodrag.clone()
	.offset({
	top: imgtodrag.offset().top,
	left: imgtodrag.offset().left
	})
	.css({
		'opacity': '0.5',
		'position': 'absolute',
		'height': '150px',
		'width': '150px',
		'z-index': '100'
	})
	.appendTo($('body')).animate({
		'top': cart.offset().top + 10,
		'left': cart.offset().left + 10,
		'width': 75,
		'height': 75
		},
		1000,
		'easeInOutExpo'
	);
	setTimeout(function () {
		cart.effect("shake", {
			times: 2
		}, 200);
	}, 1500);

	imgclone.animate({
		'width': 0,
		'height': 0
	},
	function () {
		$(this).detach()
	});
	}
	return false;
});

/*=====================================================
 S T R I N G   L I M I T ( )
=====================================================*/
function stringLimit($object, len) {
	$object.each(function(){
		if ($(this).text().length > len) {
			$(this).text($(this).text().substr(0, len));
			$(this).append(' ...');
		}
	})
}


/*=====================================================
 R E A D Y ( )
=====================================================*/
$(document).ready(function(){

	/*  String limit */
	stringLimit($('.products-grid .item .content a'), 50);
	stringLimit($('.an-article .article-info p:first-child'), 200);

	/* M e d i a   C o n t a i n e r   M o d u l e   */
	$('.media-container').mouseenter(function(){
		$(this).find('.icon').addClass('animated-05s bounceInUp');
		$(this).find('.price').addClass('animated-025s slideInDown');
		$(this).find('.saleoff').addClass('animated-025s slideInUp');
		$(this).find('.price-wrapper').css('margin-left', -(($(this).find('.price-wrapper').outerWidth()) / 2 + 1.5)).css('margin-top', -(($(this).find('.price-wrapper').outerHeight()) / 2));
	});
	$('.media-container').mouseleave(function(){
		$(this).find('.icon').removeClass('animated-05s bounceInUp');
		$(this).find('.price').removeClass('animated-025s slideInDown');
		$(this).find('.saleoff').removeClass('animated-025s slideInUp');
	});

	/* N i v o   L i g h t b o x */
	$('a.nivo-lightbox').nivoLightbox({
		effect: 'fadeScale',                             // The effect to use when showing the lightbox: fade fadeScale slideLeft slideRight slideUp slideDown fall
		theme: 'default',                           // The lightbox theme to use
		keyboardNav: true,                          // Enable/Disable keyboard navigation (left/right/escape)
		clickOverlayToClose: true,                  // If false clicking the "close" button will be the only way to close the lightbox
		onInit: function(){},                       // Callback when lightbox has loaded
		beforeShowLightbox: function(){},           // Callback before the lightbox is shown
		afterShowLightbox: function(lightbox){},    // Callback after the lightbox is shown
		beforeHideLightbox: function(){},           // Callback before the lightbox is hidden
		afterHideLightbox: function(){},            // Callback after the lightbox is hidden
		onPrev: function(element){},                // Callback when the lightbox gallery goes to previous item
		onNext: function(element){},                // Callback when the lightbox gallery goes to next item
		errorMessage: 'Đã có lỗi xảy ra. Vui lòng thử lại sau ít phút.' // Error message when content can't be loaded
	});

	/*  Collapse List */
	$('.custom-collapse-list .level-1').on('click', function(){
		// $(this).find('.level-2').slideDown('fast');
		var state = $(this).find('.level-2').css('display');
		if (state === "block") {
			$(this).find('.level-2').slideUp('fast');
		} else {
			$(this).find('.level-2').slideDown('fast');;
		}
	});

	/*  Table sorter */
	$("table.tablesorter").tablesorter({debug: false});

	/* J - C a r o u s e l  G a l l e r y */
	/* auto find the first image in gallery navigation and get its src*/
	var src = $('.gallery .navigation ul li:first-child img').attr('src');
	$('.gallery .navigation ul li:first-child img').parents().find('.main-img img').attr('src',src).fadeIn();
	$('.gallery .navigation ul li:first-child img').parents().find('.main-img .hover-overlay').attr('href',src).fadeIn();

	/* Hide the grayscale filter of the first image */
	$('.gallery .navigation li:first-child .BWFilter').addClass('hidden');
	$('.gallery .navigation li:first-child .BWfade').addClass('hidden');
	/* change image when click */
	$('.gallery .navigation li').click(function(){
		var src = $(this).children('img.img-responsive').attr('src');
		$(this).parents().find('.main-img img').fadeOut(300, function(){
			$(this).attr('src',src).fadeIn(300);
		});
		$(this).parents().find('.main-img .hover-overlay').attr('href',src);
	});

	callJcarousel($('.product-preview .gallery .jcarousel'),3,3,3);
	callJcarousel($('.recommended-products .jcarousel'),4,3,2);


	/*  MAP */
	/*Change your location by replacing 20.437175,106.337944 by your coordinates*/
	function initialize() {
		var map_canvas = document.getElementById('map');
		var myLatlng = new google.maps.LatLng(20.989113,105.795641);

		var map_options = {
			center: myLatlng,
			zoom: 15,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false
		}

		var map = new google.maps.Map(map_canvas, map_options);

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title:"TinyShop"
		});
	}
	google.maps.event.addDomListener(window, 'load', initialize);
})

