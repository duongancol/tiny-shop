# Change Log

28.3.4
Add contact page; required properties for all input

28.3.3
Add register, login, blog, blog detail, recommeded products for product detail

28.3.2
Add jcarousel gallery for product detail

28.3.1
Set height for product content; lengthen product's name to test limit string; adjust footer padding

27.3.4
Add limit string

27.3.3
Add slogan header; change brightness banner; add tag footer;

27.3.2
Add product detail; half-way cart

27.3.1
Fix product grid; Fixed price centralizing; Add 2nd level for categories unordered list